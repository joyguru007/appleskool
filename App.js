import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashOne from './src/Containers/Intro_Container/SplashOne';
import SplashTwo from './src/Containers/Intro_Container/SplashTwo';
import SignIn from './src/Containers/Auth_Container/Sign_In';
import SignUp from './src/Containers/Auth_Container/Sign_Up';
import HomeScreen from './src/Containers/Home_Container';

const Stack = createStackNavigator();

const App = () => {
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#D73C5E" />
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            options={{headerShown: false}}
            name="SplashOne"
            component={SplashOne}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="SplashTwo"
            component={SplashTwo}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="SignIn"
            component={SignIn}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="SignUp"
            component={SignUp}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="HomeScreen"
            component={HomeScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
