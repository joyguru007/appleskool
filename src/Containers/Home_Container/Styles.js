import {StyleSheet, Dimensions} from 'react-native';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    height: SCREEN_HEIGHT,
  },
  carouselStyle: {
    marginTop: 20,
    marginBottom: 10,
  },
  filterStyle: {
    marginVertical: 20,
  },
  courseCardStyle: {
    marginTop: 20,
    marginBottom: 10,
  },
});

export default styles;
