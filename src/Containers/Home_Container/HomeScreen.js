import React, {Component} from 'react';
import {Text, View, ScrollView} from 'react-native';
import styles from './Styles';
import Header from './../../Components/Header';
import Carousel from './../../Components/Carousel';
import FilterBtn from './../../Components/FilterButton';
import CourseCard from './../../Components/CourseCard';
import TeacherCard from './../../Components/TeacherCard';
import Banner from './../../Components/Banner';

class HomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <ScrollView style={styles.scrollContainer}>
          <Carousel style={styles.carouselStyle} />
          <FilterBtn heading="Categories" style={styles.filterStyle} />
          <CourseCard
            style={styles.courseCardStyle}
            heading="Featured Courses"
          />
          <TeacherCard
            style={styles.courseCardStyle}
            heading="Featured Instructor Profile"
          />
          <CourseCard
            style={styles.courseCardStyle}
            heading="Top Courses in design"
          />
          <Banner />
          <CourseCard
            style={styles.courseCardStyle}
            heading="Students are viewing"
          />
          <CourseCard
            style={styles.courseCardStyle}
            heading="Top Courses in Development"
          />
        </ScrollView>
      </View>
    );
  }
}

export default HomeScreen;
