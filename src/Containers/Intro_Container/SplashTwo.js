import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
} from 'react-native';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const logo = require('./../../../assets/logo.png');

const SplashTwo = (props) => (
  <View style={styles.container}>
    {/* logo container */}
    <View style={styles.logoContainer}>
      <Image resizeMode="contain" source={logo} style={styles.logoStyle} />
    </View>
    {/* button container */}
    <View style={styles.buttonContainer}>
      {/* create acc button */}
      <TouchableOpacity
        onPress={() => props.navigation.navigate('SignUp')}
        style={styles.btnStyle}>
        <Text style={styles.btnText}>CREATE AN ACCOUNT</Text>
      </TouchableOpacity>
      {/* sign in button */}
      <TouchableOpacity
        onPress={() => props.navigation.navigate('SignIn')}
        style={styles.btnStyle}>
        <Text style={styles.btnText}>SIGN IN</Text>
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  logoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logoStyle: {
    width: SCREEN_WIDTH / 1.1,
    height: SCREEN_HEIGHT / 6,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnStyle: {
    backgroundColor: '#D73C5E',
    width: SCREEN_WIDTH * 0.9,
    height: SCREEN_HEIGHT * 0.075,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  btnText: {
    color: '#fff',
    fontSize: SCREEN_HEIGHT * 0.022,
  },
});

export default SplashTwo;
