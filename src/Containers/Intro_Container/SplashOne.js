import React, {useEffect} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  Image,
} from 'react-native';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const backgroundLogo = require('./../../../assets/shortLogo.png');
const logo = require('./../../../assets/logo.png');

const SplashOne = (props) => {
  useEffect(() => {
    setTimeout(() => {
      props.navigation.navigate('SplashTwo');
    }, 2000);
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={backgroundLogo}
        imageStyle={styles.imageStyle}
        style={styles.image}>
        <Image resizeMode="contain" source={logo} style={styles.logoStyle} />
        <ActivityIndicator
          style={styles.indicatorStyle}
          color="#fff"
          size={'large'}
        />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    backgroundColor: '#DC4F6F',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    resizeMode: 'contain',
    opacity: 0.2,
  },
  logoStyle: {
    width: SCREEN_WIDTH / 1.1,
    height: SCREEN_HEIGHT / 6,
    backgroundColor: 'rgba(255,255,255,0.4)',
  },
  indicatorStyle: {
    position: 'absolute',
    bottom: '10%',
  },
});

export default SplashOne;
