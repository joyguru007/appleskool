import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './Styles';
import CheckBox from './../../../Components/CheckBox';
import BottomBorder from './../../../Components/BottomBorder';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';

const googleLogo = require('./../../../../assets/google.png');
const facebookLogo = require('./../../../../assets/facebook.png');

class SignInScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      name: '',
      password: '',
      rememberMe: false,
    };
  }

  componentDidMount() {
    GoogleSignin.configure({
      webClientId:
        '550366455756-b5r5jq953pfgvjn4875prvhb3n3rsk2v.apps.googleusercontent.com',
      offlineAccess: true,
      forceCodeForRefreshToken: true,
    });
  }

  getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'email,picture,id, name, first_name, last_name',
      },
    };

    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('Login Info has an error:', err);
        } else {
          console.log('result:', result);
          this.setState({isLoading: false});
          this.props.navigation.navigate('HomeScreen');
        }
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };
  loginWithFacebook = () => {
    this.setState({isLoading: true});

    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      (login) => {
        if (login.isCancelled) {
          console.log('login canceled');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const accessToken = data.accessToken.toString();
            this.getInfoFromToken(accessToken);
          });
        }
      },
      (error) => {
        console.log('login fail with error: ' + console.error());
      },
    );
  };

  handleGoogleSignIn = async () => {
    this.setState({isLoading: true});
    try {
      await GoogleSignin.hasPlayServices();
      const googleUserInfo = await GoogleSignin.signIn();
      console.log('googleUserInfo', googleUserInfo);
      this.setState({isLoading: false});
      this.props.navigation.navigate('HomeScreen');
    } catch (error) {
      ToastAndroid.showWithGravity(
        'Please try again',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      this.setState({isLoading: false});
      console.log('error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log(' user cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('play services not available');
      } else {
        // some other error happened
        console.log('something went wrong');
      }
    }
  };
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color="#D73C5E" />
        </View>
      );
    }
    return (
      <ScrollView
        style={styles.scrollContainer}
        showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <Text style={styles.signInHeading}> Sign In </Text>
          <BottomBorder thickness={1.5} color="#222" borderWidth={2} />
          <Text style={styles.descText}>
            {' '}
            Fill up the below field to access your account{' '}
          </Text>
          {/* name input */}
          <View style={styles.inputContainer}>
            <AntDesign name="contacts" color="#D73C5E" size={20} />
            <TextInput
              style={styles.inputStyle}
              placeholder="Name"
              autoCapitalize="none"
              keyboardType="default"
              secureTextEntry={false}
              value={this.state.name}
              onChangeText={(name) => this.setState({name})}
            />
          </View>
          {/* password input */}
          <View style={styles.inputContainer}>
            <Ionicons name="key-sharp" color="#D73C5E" size={20} />
            <TextInput
              style={styles.inputStyle}
              placeholder="Password"
              autoCapitalize="none"
              keyboardType="default"
              secureTextEntry={true}
              value={this.state.password}
              onChangeText={(password) => this.setState({password})}
            />
          </View>
          {/* remember me and forgot password */}
          <View style={styles.forgotPassRememberMeContainer}>
            {/* checkbox */}
            <CheckBox
              message="Remember me"
              onCheckboxClick={(val) => this.setState({rememberMe: val})}
              textColor="#333"
              textSize={15}
            />

            {/* forgot pass */}
            <TouchableOpacity style={styles.forgetPassBtn}>
              <Text style={styles.forgetPassBtnText}>Fotgot password!</Text>
            </TouchableOpacity>
          </View>
          {/* sign in button */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('HomeScreen')}
            style={styles.btnStyle}>
            <Text style={styles.btnText}>SIGN IN</Text>
          </TouchableOpacity>
          {/* or container */}
          <View style={styles.orContainer}>
            <View style={styles.borderStyle} />
            <Text style={styles.orText}>OR</Text>
            <View style={styles.borderStyle} />
          </View>
          {/* facebook btn */}
          <TouchableOpacity
            onPress={this.loginWithFacebook}
            style={styles.socialBtnStyle}>
            <Image source={facebookLogo} style={styles.sociaIconStyle} />
            <Text style={styles.socialBtnText}>Sign up with Facebook</Text>
          </TouchableOpacity>
          {/* google btn */}
          <TouchableOpacity
            onPress={this.handleGoogleSignIn}
            style={styles.socialBtnStyle}>
            <Image source={googleLogo} style={styles.sociaIconStyle} />
            <Text style={styles.socialBtnText}>Sign up with Facebook</Text>
          </TouchableOpacity>
          {/* sign up button */}
          <View style={styles.signUpBtnContainer}>
            <Text style={styles.descText}> Don't have an account?</Text>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('SignUp')}
              style={{marginLeft: 10}}>
              <Text style={[styles.descText, {fontWeight: 'bold'}]}>
                Sign Up
              </Text>
            </TouchableOpacity>
          </View>
          {/* <View style={{height: 20}} /> */}
        </View>
      </ScrollView>
    );
  }
}

export default SignInScreen;
