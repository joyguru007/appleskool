import {StyleSheet, Dimensions} from 'react-native';
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    height: SCREEN_HEIGHT,
  },
  signInHeading: {
    fontSize: SCREEN_HEIGHT * 0.035,
    fontWeight: 'bold',
    paddingBottom: 10,
    marginTop: 20,
    color: '#222',
  },
  descText: {
    fontSize: SCREEN_HEIGHT * 0.02,
    marginTop: 10,
    color: '#333',
    marginBottom: 10,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#979797',
    width: SCREEN_WIDTH * 0.8,
    marginTop: 15,
  },
  inputStyle: {
    paddingLeft: 12,
    width: SCREEN_WIDTH * 0.7,
  },
  forgotPassRememberMeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: SCREEN_WIDTH * 0.8,
    marginTop: 15,
  },
  checkBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  forgetPassBtn: {
    marginBottom: 10,
  },
  forgetPassBtnText: {
    fontSize: 15,
    marginTop: 10,
    color: '#333',
  },
  btnStyle: {
    backgroundColor: '#D73C5E',
    width: SCREEN_WIDTH * 0.8,
    height: SCREEN_HEIGHT * 0.06,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  btnText: {
    color: '#fff',
    fontSize: SCREEN_HEIGHT * 0.022,
  },
  orContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 25,
  },
  borderStyle: {
    borderWidth: 1,
    borderColor: '#eee',
    width: SCREEN_WIDTH * 0.33,
  },
  orText: {
    color: '#222',
    fontSize: SCREEN_HEIGHT * 0.022,
    paddingHorizontal: 15,
  },
  borderStyle: {
    borderWidth: 1,
    borderColor: '#eee',
    width: SCREEN_WIDTH * 0.33,
  },
  socialBtnStyle: {
    backgroundColor: '#fff',
    width: SCREEN_WIDTH * 0.8,
    height: SCREEN_HEIGHT * 0.07,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: 5,
    borderRadius: 4,
  },
  sociaIconStyle: {
    height: 35,
    width: 35,
  },
  socialBtnText: {
    color: '#222',
    fontSize: SCREEN_HEIGHT * 0.022,
    paddingHorizontal: 15,
  },
  signUpBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    bottom: SCREEN_HEIGHT / 20,
  },
});

export default styles;
