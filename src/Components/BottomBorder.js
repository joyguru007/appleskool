import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default function BottomBorder(props) {
  return (
    <View
      style={[
        styles.container,
        {
          height: props.thickness,
        },
      ]}>
      <View
        style={{
          borderWidth: props.thickness,
          borderColor: props.color,
          borderRadius: 4,
          width: SCREEN_WIDTH / (7 + 0 + props.borderWidth),
        }}
      />
      <View
        style={{
          borderWidth: props.thickness,
          borderColor: props.color,
          borderRadius: 4,
          width: SCREEN_WIDTH / (10 + 2 + props.borderWidth),
          marginHorizontal: 3,
        }}
      />
      <View
        style={{
          borderWidth: props.thickness,
          borderColor: props.color,
          borderRadius: 4,
          width: SCREEN_WIDTH / (16 + 4 + props.borderWidth),
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
