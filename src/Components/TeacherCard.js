import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomBorder from './BottomBorder';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const DATA = [
  {
    id: '1',
    name: 'Prakash Sarkar',
    skill: 'HTML, CSS',
    image: require('./../../assets/h1.jpg'),
    rating: 4.6,
    stydentCount: 126578,
    trainingCount: 6,
    price: 455,
    totalPrice: 6850,
  },
  {
    id: '2',
    name: 'Prakash Sarkar',
    skill: 'HTML, CSS',
    image: require('./../../assets/h2.jpg'),
    rating: 4.6,
    stydentCount: 126578,
    trainingCount: 6,
    price: 455,
    totalPrice: 6850,
  },
  {
    id: '3',
    name: 'Prakash Sarkar',
    skill: 'HTML, CSS',
    image: require('./../../assets/h3.jpg'),
    rating: 4.6,
    stydentCount: 126578,
    trainingCount: 6,
    price: 455,
    totalPrice: 6850,
  },
];

export default function TeacherCard(props) {
  const renderItem = ({item}) => (
    <TouchableWithoutFeedback>
      <View style={styles.cardContainer}>
        <Image style={styles.imgStyle} source={item.image} resizeMode="cover" />
        <Text style={styles.titleText}>{item.name}</Text>
        <Text style={styles.skillText}>{item.skill}</Text>
        <View style={styles.ratingContainer}>
          <View style={styles.ratingCountView}>
            <Icon name="star" size={14} color="#4f03b2" />
            <Text style={styles.ratingText}>{item.rating}</Text>
          </View>

          <Text style={styles.ratingCountText}>Instructor Rating</Text>
        </View>
        <Text style={styles.studentCountText}>
          {item.stydentCount} Students
        </Text>
        <Text style={styles.traingText}>{item.trainingCount} Trainings</Text>
        <View style={styles.bottomView}>
          <Text style={styles.priceText}>
            {' '}
            <Icon name="rupee" size={12} color="#95979d" /> {item.price}
          </Text>
          <Text style={styles.totalPriceText}>
            <Icon name="rupee" size={12} color="#95979d" /> {item.totalPrice}
          </Text>
          <TouchableOpacity style={styles.btnStyle}>
            <Text style={styles.btnText}>Read More</Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.headingContainer}>
        <View>
          <Text style={styles.headingText}>{props.heading}</Text>
          <BottomBorder thickness={1.5} color="orange" borderWidth={2} />
        </View>

        <TouchableOpacity>
          <Text style={styles.seeAllBtnText}>See all</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // padding: 10,
  },
  headingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  headingText: {
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 5,
  },
  seeAllBtnText: {
    fontSize: 15,
  },
  cardContainer: {
    width: SCREEN_WIDTH / 1.8,
    marginHorizontal: 10,
    elevation: 5,
    backgroundColor: '#fff',
    borderRadius: 4,
    padding: 8,
    marginVertical: 15,
  },
  imgStyle: {
    height: SCREEN_HEIGHT / 5,
    width: SCREEN_WIDTH / 1.98,
    alignSelf: 'center',
    borderRadius: 4,
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#222',
    marginTop: 8,
  },
  skillText: {
    fontSize: 14,
    color: '#333',
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  ratingCountView: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4,
    paddingHorizontal: 5,
    marginRight: 15,
  },
  ratingText: {
    color: '#4f03b2',
    paddingLeft: 5,
  },
  ratingCountText: {
    color: '#333',
  },
  studentCountText: {
    color: '#333',
    // marginTop: 8,
  },
  traingText: {
    color: '#333',
    // marginTop: 8,
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 8,
    borderTopWidth: 1,
    borderTopColor: '#eee',
    padding: 5,
  },
  priceText: {
    color: '#333',
    marginRight: 15,
  },
  totalPriceText: {
    color: '#333',
    marginRight: 15,
    textDecorationLine: 'line-through',
  },
  btnStyle: {
    backgroundColor: '#D73C5E',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  btnText: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#fff',
  },
});
