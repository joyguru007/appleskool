import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TouchableHighlight,
} from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const DATA = [
  {
    id: '1',
    image: require('./../../assets/c1.jpg'),
    title: 'Learn on your schedule',
    desc:
      'Study any topic anytime. Explore thousands of trainings starting at INR 455 each. ',
  },
  {
    id: '2',
    image: require('./../../assets/c2.jpg'),
    title: 'Learn on your schedule',
    desc:
      'Study any topic anytime. Explore thousands of trainings starting at INR 455 each.',
  },
  {
    id: '3',
    image: require('./../../assets/c3.jpg'),
    title: 'Learn on your schedule',
    desc:
      'Study any topic anytime. Explore thousands of trainings starting at INR 455 each.',
  },
];
export default function Carousel(props) {
  const renderItem = ({item}) => (
    <View style={[styles.cardContainer, props.style]}>
      <ImageBackground
        source={item.image}
        imageStyle={{borderRadius: 4}}
        style={styles.imgStyle}>
        <View style={styles.insideContainer}>
          <Text style={styles.titleText}>{item.title}</Text>
          <Text style={styles.descText}>{item.desc}</Text>
          <TouchableHighlight style={styles.btnStyle}>
            <Text style={styles.btnText}>Get Started Now</Text>
          </TouchableHighlight>
        </View>
      </ImageBackground>
    </View>
  );
  return (
    <View style={styles.container}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // padding: 10,
  },
  cardContainer: {
    height: SCREEN_HEIGHT / 5,
    width: SCREEN_WIDTH / 1.2,
    marginHorizontal: 10,
    borderRadius: 4,
  },
  imgStyle: {
    height: SCREEN_HEIGHT / 5,
    width: SCREEN_WIDTH / 1.2,
    borderRadius: 4,
  },
  insideContainer: {
    backgroundColor: 'rgba(255,255,255,0.8)',
    width: SCREEN_WIDTH / 1.5,
    borderRadius: 4,
    paddingVertical: 8,
    paddingHorizontal: 15,
    position: 'absolute',
    bottom: 10,
  },
  titleText: {
    fontSize: 15,
    fontWeight: 'bold',
    paddingBottom: 5,
  },
  descText: {
    fontSize: 10,
  },
  btnStyle: {
    backgroundColor: '#D73C5E',
    width: SCREEN_WIDTH / 4,
    height: SCREEN_HEIGHT / 25,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  btnText: {
    fontSize: 10,
    fontWeight: 'bold',
    color: '#fff',
  },
});
