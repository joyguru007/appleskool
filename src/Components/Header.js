import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;
const logo = require('./../../assets/logo.png');
const menu = require('./../../assets/menu.png');

export default function Header() {
  return (
    <View style={styles.headerContainer}>
      <TouchableOpacity>
        <Image source={menu} style={styles.menuStyle} resizeMode="contain" />
      </TouchableOpacity>
      <Image source={logo} style={styles.logoStyle} resizeMode="contain" />
      <TouchableOpacity>
        <AntDesign name="search1" color="#222" size={25} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT / 13,
    backgroundColor: '#fff',
    elevation: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  menuStyle: {
    width: SCREEN_WIDTH / 9,
  },
  logoStyle: {
    width: SCREEN_WIDTH / 2.5,
  },
});
