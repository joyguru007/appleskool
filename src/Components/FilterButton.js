import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const DATA = [
  {
    id: '1',
    title: 'IT & Software',
  },
  {
    id: '2',
    title: 'Marketing',
  },
  {
    id: '3',
    title: 'Language',
  },
];
export default function FilterButtons(props) {
  const renderItem = ({item}) => (
    <TouchableOpacity style={styles.cardContainer}>
      <Icon name="setting" size={22} color="#D73C5E" />
      <Text style={styles.titleText}>{item.title}</Text>
    </TouchableOpacity>
  );
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.headingContainer}>
        <Text style={styles.headingText}>{props.heading}</Text>
        <TouchableOpacity>
          <Text style={styles.seeAllBtnText}>See all</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // padding: 10,
  },
  headingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  headingText: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  seeAllBtnText: {
    fontSize: 15,
  },
  cardContainer: {
    marginHorizontal: 5,
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#D73C5E',
    borderRadius: 20,
    paddingVertical: 4,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 16,
    marginLeft: 5,
  },
});
