import React from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomBorder from './BottomBorder';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const DATA = [
  {
    id: '1',
    name: 'Prakash Sarkar',
    image: require('./../../assets/c3.jpg'),
    title: 'PhotoGraphy - Become a Better Photographer - Part-I',
    rating: 4.6,
    totalCount: 4565,
    price: 455,
    totalPrice: 6850,
  },
  {
    id: '2',
    name: 'Prakash Sarkar',
    image: require('./../../assets/c5.jpg'),
    title: 'PhotoGraphy - Become a Better Photographer - Part-I',
    rating: 4.6,
    totalCount: 4565,
    price: 455,
    totalPrice: 6850,
  },
  {
    id: '3',
    name: 'Prakash Sarkar',
    image: require('./../../assets/c6.jpg'),
    title: 'PhotoGraphy - Become a Better Photographer - Part-I',
    rating: 4.6,
    totalCount: 4565,
    price: 455,
    totalPrice: 6850,
  },
];

export default function CourseCard(props) {
  const renderItem = ({item}) => (
    <TouchableWithoutFeedback>
      <View style={styles.cardContainer}>
        <Image style={styles.imgStyle} source={item.image} resizeMode="cover" />
        <Text style={styles.titleText}>{item.title}</Text>
        <Text style={styles.nameText}>{item.name}</Text>
        <View style={styles.ratingContainer}>
          <View style={styles.ratingCountView}>
            <Icon name="star" size={14} color="#fff" />
            <Text style={styles.ratingText}>{item.rating}</Text>
          </View>

          <Text style={styles.ratingCountText}>({item.totalCount})</Text>
        </View>
        <View style={styles.bottomView}>
          <Text style={styles.priceText}>
            {' '}
            <Icon name="rupee" size={12} color="#95979d" /> {item.price}
          </Text>
          <Text style={styles.totalPriceText}>
            <Icon name="rupee" size={12} color="#95979d" /> {item.totalPrice}
          </Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.headingContainer}>
        <View>
          <Text style={styles.headingText}>{props.heading}</Text>
          <BottomBorder thickness={1.5} color="orange" borderWidth={2} />
        </View>

        <TouchableOpacity>
          <Text style={styles.seeAllBtnText}>See all</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // padding: 10,
  },
  headingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  headingText: {
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 5,
  },
  seeAllBtnText: {
    fontSize: 15,
  },
  cardContainer: {
    width: SCREEN_WIDTH / 1.6,
    margin: 15,
  },
  imgStyle: {
    height: SCREEN_HEIGHT / 5,
    width: SCREEN_WIDTH / 1.6,
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#222',
    marginTop: 8,
  },
  nameText: {
    fontSize: 14,
    color: '#333',
    marginTop: 8,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  ratingCountView: {
    backgroundColor: '#F3B431',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4,
    paddingHorizontal: 5,
    marginRight: 15,
  },
  ratingText: {
    color: '#fff',
    paddingLeft: 5,
  },
  ratingCountText: {
    color: '#333',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  priceText: {
    color: '#333',
    marginRight: 15,
  },
  totalPriceText: {
    color: '#333',
    marginRight: 15,
    textDecorationLine: 'line-through',
  },
});
