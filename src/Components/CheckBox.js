import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';

export default function CheckBox(props) {
  const [checkBoxActive, setcheckBoxActive] = useState(false);
  const onPressedChecked = () => {
    setcheckBoxActive(!checkBoxActive);
    props.onCheckboxClick(checkBoxActive ? 'false' : 'true');
  };
  return (
    <TouchableOpacity onPress={onPressedChecked} style={styles.outerContainer}>
      <View
        style={[
          styles.innerContainer,
          {
            backgroundColor: checkBoxActive ? '#D73C5E' : '#fff',
          },
        ]}
      />
      <Text
        style={{
          color: props.textColor,
          paddingLeft: 8,
          fontSize: props.textSize,
        }}>
        {props.message}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  innerContainer: {
    height: 15,
    width: 15,
    borderWidth: 1,
    borderColor: '#95979d',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
});
