import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const bannerBackground = require('./../../assets/h3.jpg');

export default function Banner(props) {
  return (
    <ImageBackground
      imageStyle={styles.imgStyle}
      style={[styles.bannerStyle, props.style]}
      source={bannerBackground}>
      <Text style={styles.heading}>Get personal learning recommandations</Text>
      <Text style={styles.desc}>Answer a few questions dor your top pics</Text>
      <TouchableOpacity style={styles.btnStyle}>
        <Text style={styles.btnText}>Get Started Now</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  bannerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT / 4.5,
    marginVertical: 20,
    backgroundColor: '#000',
  },
  imgStyle: {
    resizeMode: 'cover',
    opacity: 0.5,
  },
  heading: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  },
  desc: {
    fontSize: 18,
    color: '#fff',
    marginTop: 10,
    textAlign: 'center',
  },
  btnStyle: {
    backgroundColor: '#82B46D',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 4,
    marginTop: 10,
  },
  btnText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
  },
});
